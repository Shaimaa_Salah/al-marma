package com.databseSettings;

public class All_Matches_Desc {
	
	private String id;
	private String Des_Name;
	private String Sound;
	private String Time_stamp;
	
	
	public All_Matches_Desc(){
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDes_Name() {
		return Des_Name;
	}
	public void setDes_Name(String des_Name) {
		Des_Name = des_Name;
	}
	public String getSound() {
		return Sound;
	}
	public void setSound(String sound) {
		Sound = sound;
	}
	public String getTime_stamp() {
		return Time_stamp;
	}
	public void setTime_stamp(String time_stamp) {
		Time_stamp = time_stamp;
	}
}
