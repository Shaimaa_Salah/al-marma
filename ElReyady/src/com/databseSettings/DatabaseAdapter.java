package com.databseSettings;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.util.Log;

public class DatabaseAdapter {

	private SQLiteDatabase queenDataBase;

	private final Context myContext;
	private static String DB_PATH = "/data/data/com.elreyady/app_database/file__0/";
	private static String DB_NAME = "0000000000000001";

	DatabaseHelper dbHelper;
	String TAG = "Database Adapter";
	
	public final static String TABLE_PERSON = "person";

	public DatabaseAdapter(Context c) {
		myContext = c;
		dbHelper = new DatabaseHelper(myContext);
		

	}

	
	
	public GeneralSettings getUserSettings()
	{
		GeneralSettings res= new GeneralSettings();;
		Cursor c=this.select(0, TABLE_PERSON, false);
		if(c.moveToFirst())
		do {
			
			res.setID(c.getInt(0));
			res.setName(c.getString(1));
			
		} while (c.moveToNext());
		c.close();
		closeDataB(0);
		
		return res;
		
		
		
		
	}
	
	
	
	
	
	
	
	
	

	// ////////////////////////////////dealing with database

	public Cursor select(int dbNo, String table, boolean close) {

		SQLiteDatabase db = null;
	
		
		db=dbHelper.getReadableDatabase();

		Cursor c = db.query(table, null, null, null, null, null, null);

		// closeDataB(dbNo);
		return c;
	}

	public Cursor select(int dbNo, String table, boolean close, String id,
			String field) {

		SQLiteDatabase db = null;
		db=dbHelper.getReadableDatabase();

		Cursor c = db.query(table, null, field + "=?", new String[] { id },
				null, null, null);

		// closeDataB(dbNo);
		return c;
	}

	public void closeDataB(int dbNo) {
	dbHelper.close();
	}

	public Cursor insert(int dbNo, String table, String[] fields,
			String[] values, String whereCause) {
		Cursor c = null;
		try {
			Log.v(TAG, "insert data into " + table);

			SQLiteDatabase db = null;
		

			db=dbHelper.getWritableDatabase();
			ContentValues vals = new ContentValues();
			for (int i = 0; i < fields.length; i++)
				vals.put(fields[i], values[i]);

			long dd = db.insert(table, null, vals);
			
			String query = " Select " + whereCause + " from " + table
					+ " order by " + whereCause + " DESC limit 1";
			c = db.rawQuery(query, null);

		} catch (Exception e) {
			Log.v("Queen -----insert fun   ",
					"" + e.getMessage() + e.toString());
			e.printStackTrace();

		}
		closeDataB(dbNo);

		return c;
	}

	public void replace(int dbNo, String table, String clause,
			String column_clause, String[] fields, String[] values) {

		SQLiteDatabase db = null;

		db=dbHelper.getWritableDatabase();
		ContentValues vals = new ContentValues();
		int i;
		for (i = 0; i < fields.length; i++)
			vals.put(fields[i], values[i]);
		try {
			Log.v(TAG, "replace data into " + table + vals.toString());
			long id = db.update(table, vals, clause + "=?",
					new String[] { column_clause });
			Log.v("REsult replace", String.valueOf(id));
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		closeDataB(dbNo);
	}

	/*
	 * public void replace(String table, String[] fields, String[] values) {
	 * 
	 * SQLiteDatabase db = this.dbHelper.getWritableDatabase(); ContentValues
	 * vals = new ContentValues(); for (int i = 0; i < fields.length; i++)
	 * vals.put(fields[i], values[i]); try { Log.v(TAG, "replace data into " +
	 * table + vals.toString()); long id = db.replace(table, null, vals);
	 * Log.v("REsult replace", String.valueOf(id)); } catch (Exception e) {
	 * throw new RuntimeException(e.getMessage()); } this.dbHelper.close(); }
	 */

	public void delete(int dbNo, String table, String where, String[] whereArgs) {
		Log.v(TAG, "delete data from " + table);

		SQLiteDatabase db = null;

		db=dbHelper.getWritableDatabase();
		db.delete(table, where + "=?", whereArgs);
		closeDataB(dbNo);
	}

	public void deleteAll(int dbNo, String table) {

		SQLiteDatabase db = null;

		db=dbHelper.getWritableDatabase();
		db.delete(table, null, null);
		closeDataB(dbNo);
	}

	// ////////////////////////////////////////////////////
	private class DatabaseHelper extends SQLiteOpenHelper {

		/**
		 * Constructor Takes and keeps a reference of the passed context in
		 * order to access to the application assets and resources.
		 * 
		 * @param context
		 */
		public DatabaseHelper(Context context) {

			super(context, DB_NAME, null, 1);
			SQLiteDatabase.openOrCreateDatabase(DB_PATH, null);
			//super()
			// this.myContext = context;
			
		}

		

		@Override
		public void onOpen(SQLiteDatabase db) {
			
			
			super.onOpen(db);
			
			if (!db.isReadOnly()) {
				// Enable foreign key constraints
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		@Override
		public synchronized void close() {

			if (queenDataBase != null)
				queenDataBase.close();

			super.close();

		}

		@Override
		public void onCreate(SQLiteDatabase db) {

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

		// Add your public helper methods to access and get content from the
		// database.
		// You could return cursors by doing "return myDataBase.query(....)" so
		// it'd be easy
		// to you to create adapters for your views.
	}

	
}
