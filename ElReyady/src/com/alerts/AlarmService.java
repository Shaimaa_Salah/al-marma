package com.alerts;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.spec.DESKeySpec;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.databseSettings.All_Matches;
import com.databseSettings.All_Matches_Desc;
import com.databseSettings.Fav_Match_Desc;
import com.databseSettings.Favorite_Match;
import com.elreyady.ElReyadyActivity;
import com.phonegap.api.LOG;

import android.R.integer;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class AlarmService extends WakefulIntentService {

	// AlertsControl ALertsController;
	// TimeOperations tOperations = new TimeOperations();

	public AlarmService() {
		super("Gadwel Alerts Service v2");
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.queen.alerts.WakefulIntentService#onHandleIntent(android.content.
	 * Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		// ALertsController = new AlertsControl();
		super.onHandleIntent(intent);
		InvokeNotificationIfExistsAny();

	}

	public void clearNotifications() {
		NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		mgr.cancel(RunNotification.NOTIFICATION_NUMBER);
		RunNotification.Count = 0;

	}

	public void write_mp3_file() {

		int count;

		try {

			URL url = new URL(
					"http://38.121.76.242/riady_admin/voice/default.mp3");
			URLConnection conexion = url.openConnection();
			conexion.connect();
			// this will be useful so that you can show a tipical 0-100%
			// progress bar
			int lenghtOfFile = conexion.getContentLength();

			// downlod the file
			InputStream input = new BufferedInputStream(url.openStream());

			// OutputStream output = new
			// FileOutputStream("/sdcard/Download/nameofthefile.mp3");
			// OutputStream output = new
			// FileOutputStream("/com/elreyady/data/nameofthefile.mp3");

			// is =
			// getClass().getResourceAsStream("/com/elreyady/data/nameofthefile.mp3");
			// InputStreamReader reader = new InputStreamReader(is, "UTF-8");

			OutputStream output = openFileOutput("nameofthefile.mp3",
					this.MODE_PRIVATE);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();
		} catch (Exception e) {

//			Log.v("exception", e.getMessage());
//			e.printStackTrace();

		}
	}

	public String read_file(String file) {
		String line = null;
		String res = null;
		try {

			File outputFile = new File(getFilesDir(), file);
			if (outputFile.exists()) {
				InputStream in = openFileInput(file);

				if (in != null) {
					InputStreamReader input = new InputStreamReader(in);
					BufferedReader buffreader = new BufferedReader(input);
					res = "";
					while ((line = buffreader.readLine()) != null) {
						res += line;
					}
					in.close();
//					Toast.makeText(getApplicationContext(),
//							"File Data == " + res, Toast.LENGTH_SHORT).show();
				} else {
					/* Do something */
				}
			} else {
				/* Do nothing */
			}

		} catch (Exception e) {
//			Toast.makeText(getApplicationContext(),
//					e.toString() + e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		return res;
	}

	public void writ_file(String file, String word) {
		FileOutputStream fos = null;
		try {

			fos = openFileOutput(file, this.MODE_PRIVATE);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			fos.write(word.getBytes());
			fos.close();
		} catch (IOException e) {
//			Log.e("Controller",
//					e.getMessage() + e.getLocalizedMessage() + e.getCause());
		}
	}

	public boolean checkInternetConnection() {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		// ARE WE CONNECTED TO THE NET
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	void InvokeNotificationIfExistsAny() {

		boolean netConn = checkInternetConnection();
		if (netConn == true) {
			Parser p = new Parser();
			String xml = p
					.getXmlFromUrl("http://38.121.76.242/alreydi_api/match_notification.aspx?device_id="
							+ DeviceDataControl.getDEVICE_ID() + "&max_time=0");
			String ststus = p.status(xml);

			if (ststus.equals("جميع الفرق")) {

				all_match();
				all_match_desc();
			} else if (ststus.equals("فريقي")) {
				favorite_match();
				favorite_match_desc();
			}

			news_alert();
		}

	}

	public void favorite_match() {

		Parser p = new Parser();
		String xml;

		String old_timeStamp = read_file("fav_match_time.txt");
		if (old_timeStamp == null) {
			old_timeStamp = "0x0000000000000000";
		}
		String f = DeviceDataControl.getDEVICE_ID();
		xml = p.getXmlFromUrl("http://38.121.76.242/alreydi_api/match_notification.aspx?device_id="
				+ DeviceDataControl.getDEVICE_ID()
				+ "&max_time="
				+ old_timeStamp);
		ArrayList<Favorite_Match> m = p.parse_Favorite_Match(xml);

		String match_id = "";
		String f_team = "";
		String s_team = "";
		String new_timestamp = "";

		if (m.size() > 0) {

			match_id = m.get(0).getId();
			f_team = m.get(0).getFirst_Team();
			s_team = m.get(0).getSecond_Team();
			new_timestamp = m.get(0).getTime_Stamp();

			showAlert(" مباراة" + f_team + " و " + s_team + "بعد 5 دقائق ",
					60000, 1, 1, match_id);
			writ_file("favorite_id.txt", match_id);
			writ_file("fav_match_time.txt", new_timestamp);

		}

	}

	public void favorite_match_desc() {

		Parser p = new Parser();
		String xml;

		String match = read_file("favorite_id.txt");

		if (!(match == null)) {

			String old_timeStamp = read_file("favorite_match_desc.txt");
			if (old_timeStamp == null) {
				old_timeStamp = "0x0000000000000000";
			}
			xml = p.getXmlFromUrl("http://38.121.76.242/alreydi_api/match_description_notification.aspx?match_id="
					+ match + "&max_time=" + old_timeStamp);
			ArrayList<Fav_Match_Desc> m = p.parse_Fav_Match_Desc(xml);

			String match_id = "";
			String Des_Name = "";
			String Sound = "";
			String Time_stamp = "";

			for (int i = 0; i < m.size(); i++) {

				match_id = m.get(i).getId();
				Des_Name = m.get(i).getDes_Name();
				Sound = m.get(i).getSound();

				showAlert(Des_Name, 60000, 1, 2, match_id);

			}
			if (m.size() > 0) {
				Time_stamp = m.get(0).getTime_stamp();
				writ_file("favorite_match_desc.txt", Time_stamp);
			}

		}

	}

	public void all_match() {
		Parser p = new Parser();
		String xml;

		String old_timeStamp = read_file("all_match_time.txt");
		if (old_timeStamp == null) {
			old_timeStamp = "0x0000000000000000";
		}

		xml = p.getXmlFromUrl("http://38.121.76.242/alreydi_api/match_notification.aspx?device_id="
				+ DeviceDataControl.getDEVICE_ID()
				+ "&max_time="
				+ old_timeStamp);
		ArrayList<All_Matches> m = p.parse_All_Matches(xml);

		String match_id = "";
		String f_team = "";
		String s_team = "";
		String new_timestamp = "";

		for (int i = 0; i < m.size(); i++) {

			match_id = m.get(i).getId();
			f_team = m.get(i).getFirst_Team();
			s_team = m.get(i).getSecond_Team();

			showAlert(" " + " مباراة" + " " + f_team + " و " + s_team + " "
					+ " بعد 5 دقائق" + " ", 60000, 1, 1, match_id);

		}
		if (m.size() > 0) {
			new_timestamp = m.get(0).getTime_Stamp();
			writ_file("all_match_time.txt", new_timestamp);

		}

	}

	public void all_match_desc() {

		Parser p = new Parser();
		String xml;

		String old_timeStamp = read_file("all_match_desc.txt");
		if (old_timeStamp == null) {
			old_timeStamp = "0x0000000000000000";
		}
		xml = p.getXmlFromUrl("http://38.121.76.242/alreydi_api/match_description_notification.aspx?max_time="
				+ old_timeStamp);
		ArrayList<All_Matches_Desc> m = p.parse_All_Matches_Desc(xml);

		String match_id = "";
		String Des_Name = "";
		String Sound = "";
		String Time_stamp = "";

		for (int i = 0; i < m.size(); i++) {

			match_id = m.get(i).getId();
			Des_Name = m.get(i).getDes_Name();
			Sound = m.get(i).getSound();

			showAlert(Des_Name, 60000, 1, 2, match_id);

		}
		if (m.size() > 0) {
			Time_stamp = m.get(0).getTime_stamp();
			writ_file("all_match_desc.txt", Time_stamp);
		}

	}

	public void showAlert(String word, int peroid, int type, int urlNum,
			String match_id) {
		Calendar cal = Calendar.getInstance();
		AlarmManager AlarmMgr = (AlarmManager) context
				.getSystemService(ALARM_SERVICE);
		Intent in = new Intent(context, RunNotification.class);

		RunNotification rn = new RunNotification();

		rn.InvokeNotification("المرمى", word, "للتفاصيل اضغط هنا", type,
				urlNum, match_id);

		PendingIntent PI = PendingIntent.getService(context,
				RunNotification.Count, in, 0);

		long time = cal.getTime().getTime() + peroid;
	//	Log.v("notification time", "" + time);
		AlarmMgr.set(AlarmManager.RTC_WAKEUP, time, PI);
	}

	public void news_alert() {

		// here is to run notifications

		Calendar cal = Calendar.getInstance();

		String oldNews = "0000000000000000";
		String newNews = "0000000000000000";
		// here you should check from server about changes
		String line = null;
		String res = null;
		try {

			File outputFile = new File(getFilesDir(), "news.txt");
			if (outputFile.exists()) {

				InputStream in = openFileInput("news.txt");

				if (in != null) {
					InputStreamReader input = new InputStreamReader(in);
					BufferedReader buffreader = new BufferedReader(input);
					res = "";
					while ((line = buffreader.readLine()) != null) {
						res += line;
					}
					in.close();
					// Toast.makeText(getApplicationContext(),
					// "File Data == " + res, Toast.LENGTH_SHORT).show();
				} else {
					/* Do something */
				}

			} else {
				/* Do nothing */
			}
		} catch (Exception e) {
			// Toast.makeText(getApplicationContext(),
			// e.toString() + e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		// Log.v("lllllllllllllllllllllllllllll from", "" + res);
		// Toast.makeText(context, "res  " + res, Toast.LENGTH_LONG).show();

		if (res != null) {
			oldNews = res;
		}

		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://38.121.76.242/alreydi_api/check_news.aspx?device_id="
						+ DeviceDataControl.getDEVICE_ID());

		HttpResponse response = null;
		try {
			response = httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity ht = response.getEntity();

		BufferedHttpEntity buf = null;
		try {
			buf = new BufferedHttpEntity(ht);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = buf.getContent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader r = new BufferedReader(new InputStreamReader(is));

		StringBuilder total = new StringBuilder();
		// String line;
		try {
			while ((line = r.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String responseString = total.toString();

		// Log.v("text in file", "" + responseString);

		if (responseString != null && responseString != "") {

			FileOutputStream fos = null;
			try {

				fos = openFileOutput("news.txt", this.MODE_PRIVATE);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				responseString = responseString.substring(2,
						responseString.length());
				fos.write(responseString.getBytes());
				fos.close();
			} catch (IOException e) {
				// Log.e("Controller", e.getMessage() + e.getLocalizedMessage()
				// + e.getCause());
			}

			newNews = responseString;

			// return ParseURLResponse(responseString);

			Long oldNews_comp = Long.parseLong(oldNews, 16);
			Long newNews_comp = Long.parseLong(newNews, 16);

			if (oldNews_comp < newNews_comp) {

				AlarmManager AlarmMgr = (AlarmManager) context
						.getSystemService(ALARM_SERVICE);
				Intent in = new Intent(context, RunNotification.class);
				in.putExtra("ID", "test jhjkhn");

				RunNotification rn = new RunNotification();

				rn.InvokeNotification("المرمى", "  تابع الأخبار الجديدة ",
						"للتفاصيل اضغط هنا", 1, 3, "");

				in.putExtra("Alert Time", "jkkj jnjn");
				PendingIntent PI = PendingIntent.getService(context,
						RunNotification.Count, in, 0);

				long time = cal.getTime().getTime() + (1 * 60 * 1000);
				// Log.v("notification time", "" + time);
				AlarmMgr.set(AlarmManager.RTC_WAKEUP, time, PI);

			}
		}

	}

}
