package com.alerts;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

public class BootReciever extends BroadcastReceiver {

	long Period =  60 * 1000;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		WakefulIntentService.acquireStaticLock(context);
		AlarmManager AlrmMgr = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmService.class);
		PendingIntent pendingIntent = PendingIntent
				.getService(context, 0, i, 0);

		AlrmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime(), Period, pendingIntent);

	}
	
	

}
