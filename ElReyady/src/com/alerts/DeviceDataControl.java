package com.alerts;

import android.content.Context;

public class DeviceDataControl {

	private static String DEVICE_ID;
	private static String NETWORK_MNC;
	private static String NETWORK_MMC;
	private static String NETWORK_ID;
	

	static Context context/* =(Context) MyTabActivity.this */;

	public DeviceDataControl(Context c) {
		context = c;
		android.telephony.TelephonyManager tManager = (android.telephony.TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		setDEVICE_ID(tManager.getDeviceId());

		String networkOperator = tManager.getSimOperator();
		//String networkOperator = tManager.getNetworkOperator();
		
		

		if (!networkOperator.equals(null) && !networkOperator.equals("")) {
			setNETWORK_ID(networkOperator);
			setNETWORK_MMC(networkOperator.substring(0, 3));
			setNETWORK_MNC(networkOperator.substring(3));
		}

	}

	public static void setDEVICE_ID(String dEVICE_ID) {
		DEVICE_ID = dEVICE_ID;
	}

	public static String getDEVICE_ID() {
		return DEVICE_ID;
	}

	public static void setNETWORK_MNC(String nETWORK_MNC) {
		NETWORK_MNC = nETWORK_MNC;
	}

	public static String getNETWORK_MNC() {
		return NETWORK_MNC;
	}

	public static void setNETWORK_MMC(String nETWORK_MMC) {
		NETWORK_MMC = nETWORK_MMC;
	}

	public static String getNETWORK_MMC() {
		return NETWORK_MMC;
	}

	public static void setNETWORK_ID(String nETWORK_ID) {
		NETWORK_ID = nETWORK_ID;
	}

	public static String getNETWORK_ID() {
		return NETWORK_ID;
	}

}
